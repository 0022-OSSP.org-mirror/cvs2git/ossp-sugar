
all: validate transform

validate:
	xmllint --valid --noout test.sxml

transform:
	xsltproc --nonet sxml2html.xsl test.sxml

