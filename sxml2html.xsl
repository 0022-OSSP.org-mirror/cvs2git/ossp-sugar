<?xml version='1.0'?>
<!-- sxml2html.xsl - XSL Stylesheet for translating Sugar XML to HTMLXML 
     Copyright (c) 2001 Ralf S. Engelschall <rse@engelschall.com> -->

<xsl:stylesheet
    version='1.0'
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.org/TR/xhtml1/transitional"
    exclude-result-prefixes="#default">

  <xsl:template match="/">
    <body> 
      <xsl:apply-templates/>
    </body> 
  </xsl:template>

  <xsl:template match="/sugar/body/h1">
    <h1><xsl:apply-templates/></h1>
  </xsl:template>

  <xsl:template match="/sugar/head/abstract">
      <xsl:text>Abstract</xsl:text>
      <br/>
      <blockquote>
      <xsl:apply-templates/>
      </blockquote>
  </xsl:template>

</xsl:stylesheet>

