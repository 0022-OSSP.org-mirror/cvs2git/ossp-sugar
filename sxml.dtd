<!-- =========================================================== 
     sxml.dtd - Document Type Description (DTD) for Sugar XML 
     Copyright (c) 2001 Ralf S. Engelschall <rse@engelschall.com> 
     =========================================================== -->

<!-- :::: Formal public identifier to identify this DTD :::: -->

<!ENTITY % SXML.version "-//OSSP//DTD Sugar XML 0.1.0//EN" >

<!-- :::: Include ISO document character entitities :::: -->

<!ENTITY % ISO.entities.load "INCLUDE">
<![%ISO.entities.load;[
  <!ENTITY % ISO.entities SYSTEM "sxml.iso">
  %ISO.entities;
]]>

<!-- :::: Define local DTD parameter entities :::: -->

<!ENTITY % class.emph
    "em|it|bf|sf|sl|tt">
<!ENTITY % class.inline
    "(#PCDATA|%class.emph;)*">
<!ENTITY % class.heading
     "h1|h2|h3|h4">

<!-- :::: DTD rule set :::: -->

<!--
   sugar:
     h(ea)d
       ti(tle)
       au(thor)
       da(te)
       ab(stract)
     b(o)d(y)
       h1
         text
         h2
           text
           h3
             text
             h4
               text
   text:
     ul
     ol
     dl
       li
         text
     ta
       tr
         tc
           text
     fi(gure)

   formatting
     ul
     bf
     it
     tt
     bo(xed)

     in(dent)
     pf (preformatted)
     al(ign)

     an(chor)
     lk (link)
     
     pg (paragraph)
     br (break)

  <su>
    <hd>
      <ti>..</ti>
      <au>..</au>
      <da>..</da>
      <ab>
         ....
      </ab>
    </hd>
    <bd>
      <h1>...</h1>
      foo <it>bar</it> ..
      <h2><an name="bar"/></h2>
      <pg/>
      <ta>
        <tr>
          <tc>foo</tc>
          <tc>foo</tc>
        </tr>
      </ta>
      <an href="#bar">foo</an>
    </bd>
  </su>

-->

<!ELEMENT  sugar       (head?,body)>

<!ELEMENT  head        ((title|author|date|abstract)*)>
<!ELEMENT  body        ANY>

<!ELEMENT  title       (#PCDATA)>
<!ELEMENT  author      (#PCDATA)>
<!ELEMENT  date        (#PCDATA)>
<!ELEMENT  abstract    ANY>


<!-- headers of levels 1-4 -->
<!ELEMENT  h1          (#PCDATA)>
<!ELEMENT  h2          (#PCDATA)>
<!ELEMENT  h3          (#PCDATA)>
<!ELEMENT  h4          (#PCDATA)>

<!-- emphasized text -->
<!ELEMENT  em          (#PCDATA)>

<!-- tele-typed text -->
<!ELEMENT  tt          (#PCDATA)>

